package com.dcc052.more.aqui.app;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dcc052.more.aqui.app.entity.DBConstants;
import com.dcc052.more.aqui.app.entity.Estate;
import com.dcc052.more.aqui.app.entity.EstatesData;

/**
 * Essa classe é responsável pelo comportamento da tela de inserção de registros
 * do aplicativo.
 * 
 * @author Joao Montandon
 * @author Hudson Borges
 */
public class InsertActivity extends Activity implements OnClickListener {
	
	public static final String ERROR_MESSAGE = "Não foi possível inserir o imóvel.";
	public static final String SUCCESS_MESSAGE = "Imóvel inserido com sucesso.";

	/**
	 * Objeto que será utilizado para inserir os registros no banco de dados da
	 * aplicação.
	 */
	private EstatesData estatesDb;

	/**
	 * Botão de confirmação de inserção do registro.
	 */
	private Button okbtn;

	/**
	 * 
	 */
	private Toast defaultToast;

	/**
	 * Objeto que armazena o serviço de vibração do aparelho.
	 */
	private Vibrator defaultVibrator;


	/**
	 * Objeto que armazena conexões com a base de dados
	 */
	private SQLiteDatabase db;

	/**
	 * Objeto que armazena o serviço de localização
	 */
	private LocationManager locationManager;

	/**
	 *Objeto que armazena o nome do melhor provedor atual 
	 */
	private String bestProviderName;

	/**
	 * Objeto que adiciona critérios de seleção para provedores
	 */
	private Criteria criteria;
	private View layout;

	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_insert);

		estatesDb = new EstatesData(this);
		okbtn = (Button) findViewById(R.id.btn_ins_ok);
		okbtn.setOnClickListener(this);

	}

	/**
	 * Método que trata os eventos de click da tela.
	 * 
	 * @param v
	 *            Objeto que disparou o evento de click
	 * 
	 */
	public final void onClick(final View v) {

		boolean podeCadastrar = true;

		// Recupera os componentes da interface
		EditText text = (EditText) findViewById(R.id.txt_ins_phone);
		CheckBox checkBox = (CheckBox) findViewById(R.id.chk_ins_construction);
		RadioGroup typeRadio = (RadioGroup) findViewById(R.id.rdg_ins_type);
		RadioGroup sizeRadio = (RadioGroup) findViewById(R.id.rdg_ins_size);

		// verifica se o usuário digitou o número corretamente
		for (char c : text.getText().toString().toCharArray()) {
			if (c < '0' || c > '9') {
				podeCadastrar = false;
			}
		}

		// recupera os valores preenchidos no componentes
		if (text.getText().toString().length() > 0 && podeCadastrar) {
			Integer phone = Integer.parseInt(text.getText().toString());
			String type = ((RadioButton) findViewById(typeRadio
					.getCheckedRadioButtonId())).getText().toString();
			String size = ((RadioButton) findViewById(sizeRadio
					.getCheckedRadioButtonId())).getText().toString();
			String inConstruction = checkBox.isChecked() + "";

			double location[] = getLocation();

			// Cria instância do objeto a ser inserido
			Estate estate = new Estate(type, size, phone, inConstruction, location[0], location[1]);

			// Insere o novo objeto
			addEstate(estate);

		} else {
			// Envia mensagem de erro informando que os dados estão inválidos
			makeAndShowToast("Dados inválidos.");
		}
	}

	/**
	 * Método que retorna a localização atual do aparelho.
	 * 
	 * @return valores de altitude e latitude, respectivamente.
	 * 
	 */
	public double[] getLocation () {

		/**
		 * =--> INICIO TAREFA TUTORIAL - OBTENÇÃO DE LOCALIZAÇÃO
		 * 
		 * -----------------------
		 * Contexto da tarefa
		 * -----------------------
		 * 
		 * No momento em que o usuário realiza a inserção de um imóvel, o aplicativo  
		 * coleta informações de localização no serviço de localização provido pelo 
		 * aparelho, como a última localização registrada pelo provedor. 
		 * Mais especificamente, este aplicativo necessita somente dos últimos valores 
		 * de latitude e longitude registrados pelo provedor.
		 * 	    
		 * -----------------------
		 * Problema
		 * -----------------------
		 * 
		 * Aparentemente o desenvolvedor responsável não implementou corretamente os 
		 * requisitos necessários e o código não está sendo aceito nos testes automatizados. 
		 * Sabe-se que o problema encontra-se neste trecho indicado pela tarefa.
		 * 
		 * -----------------------
		 * Instruções Gerais
		 * -----------------------
		 * 
		 * Você deve analisar o código e com as informações do teste automatizado fazer 
		 * a(s) modificação(ões) necessária(s) para que o código seja aceito no teste, dadas 
		 * as seguintes informações:
		 * 
		 * 	- A localização deve ser obtida do melhor provedor disponível;
		 *  - A definição de melhor provedor é dado pelos seguintes critérios:
		 *  	* O provedor preferencialmente deve ter boa acurácia
		 *  	* O provedor preferencialmente deve ter baixo consumo de energia
		 *  	* O uso do provedor não deve acarretar em custos monetários  
		 * 	- Caso não haja provedores disponíveis, deve-se retornar o valor padrão 0 para 
		 * latitude e longitude.
		 * 
		 * Sabe-se também que as variáveis 'locationManager', 'criteria' e 'bestProviderName' 
		 * são necessárias e devem ter seus valores atualizados na tarefa.
		 * 
		 * Para a realização desta tarefa você deverá utilizar a documentação provida 
		 * pela plataforma APIMiner, e pode ser acessada pelo endereço:
		 * 
		 * -> http://java.labsoft.dcc.ufmg.br/apiminer/static/docs/reference/packages.html
		 * 
		 * A busca por classes específicas pode ser feita digitando o nome da classe 
		 * no campo de busca presente canto superior direito na página (lupa).
		 * 
		 * As classes envolvidas nesta atividade são:
		 * 
		 * = android.location.LocationManager
		 * = android.location.Criteria
		 * = android.location.Location
		 * = android.app.Activity
		 * 
		 * O teste responsável por verificar a validade do código encontra-se no projeto 
		 * "More Aqui App Test 2" e tem o nome "com.dcc052.more.aqui.app.test.TarefaTutorialTest.java".
		 * Para executá-lo clique com o botão direito sobre o arquivo e selecione a opção
		 * "Run As" > "Android JUnit Test".
		 * 
		 * Não esqueça de registrar a hora e minuto de início da tarefa no formulário.
		 * 
		 */

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		criteria = new Criteria(); 
		criteria.setAccuracy(Criteria.ACCURACY_FINE); 
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		criteria.setCostAllowed(false);

		bestProviderName = locationManager.getBestProvider(criteria, true);

		Location location = locationManager.getLastKnownLocation(bestProviderName);
		
		double latitude = 0;
		double longitude = 0;
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		} 
		
		return new double[]{latitude, longitude};

		/**
		 * =--> FIM TAREFA TUTORIAL
		 */

	}

	/**
	 * Método que adiciona um objeto no banco de dados.
	 * 
	 * @param estate
	 *            Objeto que será persistido no banco
	 */
	public void addEstate(final Estate estate) {

		/** 
		 * =--> INICIO TAREFA UM - INSERINDO UM IMÓVEL
		 * 
		 * ------------------
		 * Contexto da tarefa
		 * ------------------
		 * 
		 * Após o usuário preencher as informações e clicar em "Pronto!", os dados são 
		 * validados e inseridos. Neste processo é necessário que os dados sejam devidamente 
		 * inseridos, evitando que os dados sejam corrompidos durante o processo. 
		 * Nesse sentido, esta tarefa tem por objetivo garantir que os dados (já validados) 
		 * sejam devidamente inseridos.
		 *  
		 * ------------------
		 * Problema
		 * ------------------
		 * 
		 * Aparentemente o desenvolvedor responsável não implementou corretamente os 
		 * requisitos necessários e o código não está sendo aceito nos testes automatizados. 
		 * Sabe-se que o problema encontra-se neste trecho indicado pela tarefa.
		 * 
		 * ------------------
		 * Instruções Gerais
		 * ------------------
		 * 
		 * Você deve analisar o código e com as informações do teste automatizado fazer 
		 * a(s) modificação(ões) necessária(s) para que o código seja aceito no teste, dadas 
		 * as seguintes informações:
		 * 
		 * 	- A variável de classe 'estatesDb' fornece instâncias da base de dados para 
		 *    inserção dos objetos;
		 *  - O nome da tabela onde os valores serão inseridos está armazenado na constante
		 *    'DBConstants.TABLE_NAME'.
		 *  - A manipulação da base de dados deve ser feita de forma segura, ou seja, 
		 *    todos os dados devem ser persistidos somente se não houverem erros durante 
		 *    o processo;
		 *  - Uma mensagem deverá ser apresentada ao final do processo, informando se o
		 *    processo foi concluído com sucesso ou não. O método makeAndShowToast(String)
		 *    provido por esta classe deve ser utilizado.   
		 * 
		 * A variável de classe 'db' é necessária e deve ter seu valor atualizado na tarefa.
		 * 
		 * O método 'makeAndShowToast(String)' desta classe deve ser utilizada para apresentação 
		 * de mensagens ao usuário.
		 * 
		 * As variaveis 'ERROR_MESSAGE' e 'SUCCESS_MESSAGE' contém, respectivamente, as mensagens 
		 * de erro e sucesso a serem apresentadas ao usuário. 
		 * 
		 * Para a realização desta tarefa você deverá utilizar a documentação provida 
		 * pela própria API, e pode ser acessada pelo endereço:
		 * 
		 * -> http://developer.android.com/reference/packages.html
		 * 
		 * A busca por classes específicas pode ser feita digitando o nome da classe 
		 * no campo de busca presente canto superior direito na página (lupa).
		 * 
		 * As classes envolvidas nesta atividade são:
		 * 
		 * = android.database.sqlite.SQLiteDatabase
		 * = android.database.sqlite.SQLiteClosable
		 * = android.content.ContentValues
		 * 
		 * O teste responsável por verificar a validade do código encontra-se no projeto 
		 * "More Aqui App Test 2" e tem o nome "com.dcc052.more.aqui.app.test.TarefaUmTest.java".
		 * Para executá-lo clique com o botão direito sobre o arquivo e selecione a opção
		 * "Run As" > "Android JUnit Test".
		 * 
		 * Não esqueça de registrar a hora e minuto de início da tarefa no formulário.
		 * 
		 */
		
		ContentValues values = new ContentValues();
		values.put(DBConstants.PHONE, estate.PHONE);
		values.put(DBConstants.SIZE, estate.SIZE);
		values.put(DBConstants.STATUS, estate.STATUS);
		values.put(DBConstants.TYPE, estate.TYPE);
		values.put(DBConstants.LAT, estate.LAT);
		values.put(DBConstants.LONG, estate.LONG);

		db = estatesDb.getWritableDatabase();
		db.beginTransaction();
		try {
			db.insertOrThrow(DBConstants.TABLE_NAME, null, values);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			makeAndShowToast(ERROR_MESSAGE);
		} finally {
			db.endTransaction();
			db.releaseReference();
		}
			
		makeAndShowToast(SUCCESS_MESSAGE);
		
		/** 
		 * =--> FIM TAREFA UM 
		 */

	}


	/**
	 * 
	 * Método que apresenta uma mensagem customizada ao usuário 
	 * 
	 * @param text 
	 * 			texto a ser exibido na notificação
	 */
	public void makeAndShowToast(final String text) {

		/**
		 * =--> INICIO TAREFA DOIS - EXIBINDO MENSAGENS AO USUÁRIO
		 * 
		 * ------------------
		 * Contexto da tarefa
		 * ------------------
		 * 
		 * Para diversas ações dos usuários é necessário apresentar uma resposta 
		 * para o usuário. Assim, esta atividade engloba os códigos necessários
		 * para apresentação de uma determinada mensagem ao usuário.
		 *  
		 * ------------------
		 * Problema
		 * ------------------
		 * 
		 * Aparentemente o desenvolvedor responsável não implementou corretamente os 
		 * requisitos necessários e o código não está sendo aceito nos testes automatizados. 
		 * Sabe-se que o problema encontra-se neste trecho indicado pela tarefa.
		 * 
		 * ------------------
		 * Instruções Gerais
		 * ------------------
		 * 
		 * Você deve analisar o código e com as informações do teste automatizado fazer 
		 * a(s) modificação(ões) necessária(s) para que o código seja aceito no teste, dadas 
		 * as seguintes informações:
		 * 
		 * - Todas mensagens devem ser apresentadas ao usuário utilizando um layout customizado 
		 *   para a classe android.widget.Toast;
		 * - Além da mensagem visual é necessário fazer com que o aparelho vibre por
		 *   500 milisegundos;   
		 * - As mensagens devem ter uma duração longa na tela do aparelho;   
		 * - O texto deve aparecer no centro e na horizontal do aparelho.
		 * 
		 * As variáveis 'defaultToast' e 'defaultVibrator' são necessárias e devem ter seus
		 * valores atualizados na tarefa.
		 * 
		 * A variável 'layout' contém a visão do objeto customizado a ser apresentado.
		 * 
		 * Para a realização desta tarefa você deverá utilizar a documentação provida 
		 * pela plataforma APIMiner, e pode ser acessada pelo endereço:
		 * 
		 * -> http://java.labsoft.dcc.ufmg.br/apiminer/static/docs/reference/packages.html
		 * 
		 * A busca por classes específicas pode ser feita digitando o nome da classe 
		 * no campo de busca presente canto superior direito na página (lupa).
		 * 
		 * As classes envolvidas nesta atividade são:
		 * 
		 * = android.widget.Toast
		 * = android.widget.TextView
		 * = android.os.Vibrator
		 * = android.view.LayoutInflater
		 * = android.content.Context
		 * 
		 * O teste responsável por verificar a validade do código encontra-se no projeto 
		 * "More Aqui App Test 2" e tem o nome "com.dcc052.more.aqui.app.test.TarefaDoisTest.java".
		 * Para executá-lo clique com o botão direito sobre o arquivo e selecione a opção
		 * "Run As" > "Android JUnit Test".
		 * 
		 * Não esqueça de registrar a hora e minuto de início da tarefa no formulário.
		 * 
		 */
		
		layout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.toast_layout, null, false);

		TextView textView = (TextView) layout.findViewById(R.id.text);
		textView.setText(text);

		defaultToast = new Toast(getApplicationContext());
		defaultToast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
		defaultToast.setDuration(Toast.LENGTH_LONG);
		defaultToast.setView(layout);
		defaultToast.show();

		defaultVibrator = (Vibrator) getBaseContext().getSystemService(Context.VIBRATOR_SERVICE);
		if (defaultVibrator != null && defaultVibrator.hasVibrator()) {
			defaultVibrator.vibrate(500);
		}
		
		/**
		 * =--> FIM TAREFA DOIS
		 */

	}

	/**
	 * @return
	 */
	public Toast getDefaultToast() {
		return defaultToast;
	}

	public SQLiteDatabase getDb() {
		return db;
	}

	public String getBestProviderName() {
		return bestProviderName;
	}

	public Criteria getCriteria() {
		return criteria;
	}

	public EstatesData getEstatesDb() {
		return estatesDb;
	}

	public View getLayout() {
		return layout;
	}

}
