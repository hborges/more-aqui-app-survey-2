package com.dcc052.more.aqui.app.entity;

import static android.provider.BaseColumns._ID;
import static com.dcc052.more.aqui.app.entity.DBConstants.LAT;
import static com.dcc052.more.aqui.app.entity.DBConstants.LONG;
import static com.dcc052.more.aqui.app.entity.DBConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.DBConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.DBConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.DBConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.DBConstants.TYPE;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author joao.montandon
 * 
 */
public class EstatesData extends SQLiteOpenHelper {

	/**
	 * database's name
	 */
	private static final String DB_NAME = "moreaquiapp.db";

	/**
	 * database's version
	 */
	private static final int DB_VERSION = 1;
	
	
	/**
	 * context
	 */
	private Context context;
	
	/**
	 * Database's constructor
	 * 
	 * @param ctx
	 */
	public EstatesData(final Context ctx) {
		super(ctx, DB_NAME, null, DB_VERSION);
		this.context = ctx;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.s#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public final void onCreate(final SQLiteDatabase db) {

		String query = "CREATE TABLE " + TABLE_NAME + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TYPE
				+ " TEXT NOT NULL, " + SIZE + " TEXT NOT NULL, " + STATUS
				+ " TEXT, " + PHONE + " TEXT NOT NULL, " + LAT + " DOUBLE, "
				+ LONG + " DOUBLE);";
		db.execSQL(query);

		Log.v(this.getClass().getName(), "Database created: " + query);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	@Override
	public final void onUpgrade(final SQLiteDatabase db, final int oldVersion,
			final int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
	}

	@Override
	public synchronized SQLiteDatabase getReadableDatabase() {
		SQLiteDatabase db = super.getReadableDatabase();
		if (db.isReadOnly()) {
			return db;
		} else { 
			return SQLiteDatabase.openDatabase(context.getDatabasePath(DB_NAME).getPath(), null, SQLiteDatabase.OPEN_READONLY);
		}	
	}

	@Override
	public synchronized SQLiteDatabase getWritableDatabase() {
		return super.getWritableDatabase();
	}
	
	public synchronized void destroyDB() {
		getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	}
	
	

}
