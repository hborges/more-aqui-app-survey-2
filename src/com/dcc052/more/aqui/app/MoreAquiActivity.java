package com.dcc052.more.aqui.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Essa classe é responsável pelo comportamento da tela principal do aplicativo.
 * 
 * @author joao.montandon
 * 
 */
public class MoreAquiActivity extends Activity implements OnClickListener {

	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_aqui);

		Button newBtn = (Button) findViewById(R.id.btn_new);
		newBtn.setOnClickListener(this);

		Button showBtn = (Button) findViewById(R.id.btn_show);
		showBtn.setOnClickListener(this);
	}

	/**
	 * Método responsável pelo tratamento do click dos botões da tela.
	 * 
	 * @param v
	 *            Elemento de interface que disparou o evento de click.
	 */
	public final void onClick(final View v) {
		
		switch (v.getId()) {

		case R.id.btn_new:
			startActivity(new Intent(this, InsertActivity.class));
			break;

		case R.id.btn_show:
			startActivity(new Intent(this, ShowActivity.class));
			break;

		default:
			break;

		}
	}

}
