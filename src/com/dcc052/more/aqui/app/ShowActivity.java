package com.dcc052.more.aqui.app;

import static android.provider.BaseColumns._ID;
import static com.dcc052.more.aqui.app.entity.DBConstants.LAT;
import static com.dcc052.more.aqui.app.entity.DBConstants.LONG;
import static com.dcc052.more.aqui.app.entity.DBConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.DBConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.DBConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.DBConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.DBConstants.TYPE;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dcc052.more.aqui.app.entity.EstatesData;

/**
 * Classe responsável pelo comportamento da tela de recuperação de registros.
 * 
 * @author joao.montandon
 */
public class ShowActivity extends ListActivity implements
		OnItemLongClickListener {

	/**
	 * Banco de dados.
	 */
	private EstatesData estateDb;

	/**
	 * Lista das colunas que serão recuperadas do banco.
	 */
	private static final String[] FROM = new String[] { _ID, PHONE, SIZE,
			STATUS, TYPE, LAT, LONG };

	/**
	 * Lista dos componentes de interface que receberão a informação do banco.
	 */
	private static final int[] TO = { R.id.phone, R.id.size, R.id.status,
			R.id.type, R.id.latitude, R.id.longitude };

	@Override
	protected final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show);
		estateDb = new EstatesData(this);

		// define o evento de click longo a ser executado pelos itens da lista
		getListView().setOnItemLongClickListener(this);

		// recupera os registros da base
		getEstates();
	}

	/**
	 * trata eventos de click longo dos itens da lista.
	 * 
	 * @param arg0
	 * @param arg1
	 *            objeto que dispatou o evento
	 * @param arg2
	 * @param arg3
	 * @return true se tudo está ok.
	 */
	public final boolean onItemLongClick(final AdapterView<?> arg0,
			final View arg1, final int arg2, final long arg3) {

		// recupera o id do comando
		LinearLayout row = (LinearLayout) arg1;
		TextView txtId = (TextView) row.getChildAt(0);
		String estateId = txtId.getText().toString();

		//remove o registro do banco
		SQLiteDatabase db = estateDb.getReadableDatabase();
		db.delete(TABLE_NAME, "_ID = " + estateId, null);
		db.close();

		// atualiza a tela e envia uma mensagem na tela informando ao usuário
		// que a remoção foi feita com sucesso.
		getEstates();
		Toast.makeText(this, "Registro removido com sucesso.",
				Toast.LENGTH_SHORT).show();
		return true;
	}

	/**
	 * Recupera os registros da base.
	 */
	private void getEstates() {

		// recupera os registros do banco
		SQLiteDatabase db = estateDb.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, FROM, null, null, null, null, null);
		startManagingCursor(cursor);

		// atrela os registros com a interface de lista de registros
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
				R.layout.activity_row, cursor, FROM, TO, 0);
		setListAdapter(adapter);
		db.close();
	}
}
